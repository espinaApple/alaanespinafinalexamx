package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterAtm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_atm);

    }

    public void register (View view){
        Intent intent = new Intent(RegisterAtm.this, LoginAtm.class);

        //Balance
        EditText BalanceEditText = (EditText) findViewById(R.id.BalanceEditText);
        String bal = BalanceEditText.getText().toString();
        //Account Number
        EditText AccountNumberEditText = (EditText) findViewById(R.id.AccountNumberEditText);
        String acc = AccountNumberEditText.getText().toString();
        //Password
        EditText PasswordEditText = (EditText) findViewById(R.id.PasswordEditText);
        String pass = PasswordEditText.getText().toString();
        //PIN
        EditText PinEditText = (EditText) findViewById(R.id.PinEditText);
        String pin = PinEditText.getText().toString();

        //VALIDATION
        if (bal.length() != 0 && acc.length() != 0 && pass.length() != 0 && pin.length() != 0){
            intent.putExtra("Balance", bal);
            intent.putExtra("Account Number", acc);
            intent.putExtra("Password", pass);
            intent.putExtra("Pin", pin);

            Toast.makeText(RegisterAtm.this, "Successfully Registered!", Toast.LENGTH_LONG).show();

            startActivity(intent);
        } else {
            Toast.makeText(RegisterAtm.this, "Complete the registration details!!!", Toast.LENGTH_SHORT).show();
        }
    }


}
