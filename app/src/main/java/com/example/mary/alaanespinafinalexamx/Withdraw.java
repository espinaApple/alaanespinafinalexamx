package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Withdraw extends AppCompatActivity {
    String bal;

    TextView balance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        Bundle bundle = getIntent().getExtras();

        bal = bundle.getString("Balance");
    }

    public void getAmount(View view){
        Intent intent = new Intent(Withdraw.this, Receipt.class);

        EditText AmountEditText = (EditText) findViewById(R.id.AmountEditText);
        String amt = AmountEditText.getText().toString();

        Toast.makeText(Withdraw.this, "Successfully Withdraw!", Toast.LENGTH_LONG).show();
        intent.putExtra("Amount", amt);

        float newBal = Float.parseFloat(bal) - Float.parseFloat(amt);
        Log.d("test", String.valueOf(newBal));
        intent.putExtra("Balance", String.valueOf(newBal));
        intent.putExtra("State", "withdraw");
        startActivity(intent);

    }


}
