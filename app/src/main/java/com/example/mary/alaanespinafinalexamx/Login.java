package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    Button RegisterBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void login(View view){
        //Username
        EditText UsernameEditText = (EditText) findViewById(R.id.UserNameEditText);
        String uname = UsernameEditText.getText().toString();
        //Password
        EditText PasswordEditText = (EditText) findViewById(R.id.PasswordEditText);
        String pass = PasswordEditText.getText().toString();

        Intent intent = new Intent(Login.this, Menu.class);

        if (getIntent().getExtras() != null && uname.length() != 0 && pass.length() != 0
                && uname.equals(getIntent().getExtras().getString("Username"))
                && pass.equals(getIntent().getExtras().getString("Password"))) {
            intent.putExtras(getIntent().getExtras());
            intent.putExtra("Username", uname);
            intent.putExtra("Password", pass);

            startActivity(intent);
        } else {
            Toast.makeText(Login.this, "Invalid login credentials!", Toast.LENGTH_SHORT).show();
        }

    }

    public void register(View view){
        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);

    }



    public void onBackPressed(){
        moveTaskToBack(true);
    }
}
