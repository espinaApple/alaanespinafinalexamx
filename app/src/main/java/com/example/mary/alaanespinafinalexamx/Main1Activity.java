package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class Main1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        ImageView firstImage = (ImageView)findViewById(R.id.img1);
        firstImage.animate().alpha(1f).setDuration(2000).rotationBy(3600);


        Thread thread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally{
                    Intent intent = new Intent(Main1Activity.this,Main2Activity.class);
                    startActivity(intent);
                }
            }
        };
        thread.start();

    }

    public void onBackPressed(){
        moveTaskToBack(true);
    }
}
