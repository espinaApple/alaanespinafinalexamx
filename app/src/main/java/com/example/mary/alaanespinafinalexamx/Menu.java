package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Menu extends AppCompatActivity {
    TextView FirstName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        TextView FirstNameTextView = (TextView) findViewById(R.id.FirstNameTextView);
        FirstNameTextView.setText(Account.name);


    }



    public void animation(View view){
        Intent intent = new Intent(Menu.this, Main1Activity.class);
        startActivity(intent);
    }

    public void atm(View view) {
        Intent intent = new Intent(Menu.this, SplashAtm.class);
        startActivity(intent);
    }

    public void triangular(View view){
        Intent intent = new Intent(Menu.this, Triangular.class);
        startActivity(intent);
    }

    public void tictactoe(View view){
        Intent intent = new Intent(Menu.this, SplashScreen.class);
        startActivity(intent);
    }


}
