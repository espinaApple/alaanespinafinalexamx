package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void register (View view){
        Intent intent = new Intent(Register.this, Login.class);

        //First Name
        EditText FirstNameEditText = (EditText) findViewById(R.id.FirstNameEditText);
        String fname = FirstNameEditText.getText().toString();
        Account.name = FirstNameEditText.getText().toString();
        //Last Name
        EditText LastNameEditText = (EditText) findViewById(R.id.LastNameEditText);
        String lname = LastNameEditText.getText().toString();
        //Username
        EditText UserNameEditText = (EditText) findViewById(R.id.UserNameEditText);
        String uname = UserNameEditText.getText().toString();
        //Password
        EditText PasswordEditText = (EditText) findViewById(R.id.PasswordEditText);
        String pass = PasswordEditText.getText().toString();

        //VALIDATION
        if (fname.length() != 0 && lname.length() != 0 && uname.length() != 0 && pass.length() != 0){
            intent.putExtra("First Name", fname);
            intent.putExtra("Last Name", lname);
            intent.putExtra("Username", uname);
            intent.putExtra("Password", pass );

            Toast.makeText(Register.this, "Successfully Registered!", Toast.LENGTH_LONG).show();

            startActivity(intent);
        } else {
            Toast.makeText(Register.this, "Complete the registration details!!!", Toast.LENGTH_SHORT).show();
        }
    }
}
