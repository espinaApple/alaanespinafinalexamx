package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class Receipt extends AppCompatActivity {
    TextView balance;
    TextView amount;
    TextView textView11;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        //Bundle bundle = getIntent().getExtras();
        Intent intent = getIntent();
        String bal = intent.getExtras().getString("Balance");
        //String bal = bundle.getString("Balance");
        String amt = intent.getExtras().getString("Amount");

        String state = intent.getExtras().getString("State");

        balance = (TextView) findViewById(R.id.balance);
        amount = (TextView) findViewById(R.id.amount);
        textView11 = (TextView) findViewById(R.id.textView11);

        if (state.equals("check_bal")) {
            balance.setText(String.format(Locale.ENGLISH, "%s : %s", balance.getText(), bal));
        } else if (state.equals("withdraw")) {
            amount.setVisibility(View.VISIBLE);
            textView11.setVisibility(View.VISIBLE);
            balance.setText(String.format(Locale.ENGLISH, "%s : %s", balance.getText(), bal));
            amount.setText(String.format(Locale.ENGLISH, "%s : %s", amount.getText(), amt));

        }
    }

    public void checkBalance(View view){
        Intent intent = new Intent(Receipt.this, Pin.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }
}
