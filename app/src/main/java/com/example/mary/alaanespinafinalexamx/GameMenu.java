package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class GameMenu extends AppCompatActivity {
    private CheckBox chkbx;
    EditText BO;
    Button Play;
    Button rbtn;

    private int r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_menu);

        chkbx = (CheckBox) findViewById(R.id.checkBox);
        BO = (EditText) findViewById(R.id.BestOfEditText);
        Play = (Button) findViewById(R.id.button2);
        rbtn = (Button) findViewById(R.id.RandomButton);

    }


    public  void onClick(View view){
        if(chkbx.isChecked()) {

            BO.setVisibility(View.VISIBLE);
            Play.setVisibility(View.VISIBLE);
            rbtn.setEnabled(false);
        }
        else{
            BO.setVisibility(View.INVISIBLE);
            Play.setVisibility(View.INVISIBLE);
            rbtn.setEnabled(true);
        }
    }



    public void random(View view){

        EditText TomEditText = (EditText) findViewById(R.id.TomEditText);
        String t = TomEditText.getText().toString();

        EditText JerryEditText = (EditText) findViewById(R.id.JerryEditText);
        String j = JerryEditText.getText().toString();

        Intent intent = new Intent(GameMenu.this, GameLogic.class);

        if (t.length() != 0 && j.length() != 0){
            intent.putExtra("Tom", t);
            intent.putExtra("Jerry", j);

            r = 0;
            int[] intArray = {3, 5, 7};
            int idx = new Random().nextInt(3);
            r = intArray[idx];
            StringBuilder builder = new StringBuilder();
            builder.append(r);
            Toast.makeText(GameMenu.this, builder, Toast.LENGTH_SHORT).show();

            intent.putExtra("RandomVal",r);

            startActivity(intent);
        } else {
            Toast.makeText(GameMenu.this, "Complete the details!!!", Toast.LENGTH_SHORT).show();
        }


    }

    public void bestof(View view){
        EditText TomEditText = (EditText) findViewById(R.id.TomEditText);
        String t = TomEditText.getText().toString();

        EditText JerryEditText = (EditText) findViewById(R.id.JerryEditText);
        String j = JerryEditText.getText().toString();

       // EditText BestOfEditText = (EditText) findViewById(R.id.BestOfEditText);
        String bo = BO.getText().toString();
        int bo1 = Integer.parseInt(bo);


        Intent intent = new Intent(GameMenu.this, GameLogic.class);
        r = 0;
        int[] intArray = {3, 5, 7};

        if(t.length() !=0 && j.length() !=0 && bo.length() != 0){
            if (t.length() != 0 && j.length() != 0){
                intent.putExtra("Tom", t);
                intent.putExtra("Jerry", j);



                if (bo.length() != 0 && bo1 == intArray[0] || bo1 == intArray[1] || bo1 == intArray[2]){

                    Toast.makeText(GameMenu.this, bo, Toast.LENGTH_SHORT).show();

                    startActivity(intent);

                } else {
                    Toast.makeText(GameMenu.this, "Invalid Best Of Value", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(GameMenu.this, "Complete the details!!!", Toast.LENGTH_SHORT).show();
            }

        }
        else{
            Toast.makeText(GameMenu.this, "Fill Up all the Fields", Toast.LENGTH_SHORT).show();
        }


    }

    public void onBackPressed(){
        moveTaskToBack(true);
    }



}
