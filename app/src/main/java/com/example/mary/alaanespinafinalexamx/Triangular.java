package com.example.mary.alaanespinafinalexamx;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Triangular extends AppCompatActivity {
    int value = 0;
    int start = 1;
    int MAX = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triangular);
    }

    public void Compute (View v) {
        TextView res = (TextView)findViewById(R.id.textView3);
        for (int i = start; i <= MAX; i++) {
            value = value + i;
            res.append(i + " = " + value + "\n");
        }
    }
}
