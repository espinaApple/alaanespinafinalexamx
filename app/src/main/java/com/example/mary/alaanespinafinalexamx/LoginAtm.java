package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginAtm extends AppCompatActivity {
    Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_atm);
    }

    public void login(View view){
        //Account Number
        EditText AccountNumberEditText = (EditText) findViewById(R.id.AccountNumberEditText);
        String acc = AccountNumberEditText.getText().toString();
        //Password
        EditText PasswordEditText = (EditText) findViewById(R.id.PasswordEditText);
        String pass = PasswordEditText.getText().toString();

        Intent intent = new Intent(LoginAtm.this, Pin.class);

        if (getIntent().getExtras() != null && acc.length() != 0 && pass.length() != 0
                && acc.equals(getIntent().getExtras().getString("Account Number"))
                && pass.equals(getIntent().getExtras().getString("Password"))) {
            intent.putExtras(getIntent().getExtras());
            intent.putExtra("Account Number", acc);
            intent.putExtra("Password", pass);

            startActivity(intent);
        } else {
            Toast.makeText(LoginAtm.this, "Invalid login credentials!!!", Toast.LENGTH_SHORT).show();
        }

    }

    public void display(View view){
        Intent intent = new Intent(LoginAtm.this, RegisterAtm.class);
        startActivity(intent);

    }
}
