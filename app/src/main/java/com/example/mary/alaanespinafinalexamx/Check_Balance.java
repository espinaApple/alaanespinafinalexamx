package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class Check_Balance extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check__balance);
        Intent intent = getIntent();

        String bal = intent.getExtras().getString("Balance");

        TextView Balance = (TextView) findViewById(R.id.balance);
        Balance.setText(String.format(Locale.ENGLISH, "%s", bal));
    }

    public void receiptBalance(View view){
        Intent intent = new Intent(Check_Balance.this, Receipt.class);
        intent.putExtras(getIntent().getExtras());
        intent.putExtra("State", "check_bal");
        startActivity(intent);
    }




}
