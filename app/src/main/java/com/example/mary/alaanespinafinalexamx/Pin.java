package com.example.mary.alaanespinafinalexamx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Pin extends AppCompatActivity {

    Button mCheckBalance;
    Button mWithdraw;
    EditText mPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        mCheckBalance = (Button) findViewById(R.id.button2);
        mWithdraw = (Button) findViewById(R.id.button3);
        mPin = (EditText) findViewById(R.id.PinEditText);

        mCheckBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBalance();
            }
        });

        mWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdraw();
            }
        });
    }

    public void checkBalance(){
        if (getIntent().getExtras().getString("Pin").equals(mPin.getText().toString())) {
            Intent intent = new Intent(Pin.this, Check_Balance.class);
            /*intent.putExtra("Balance", getIntent().getExtras().getString("Balance"));
            intent.putExtra("Account Number", getIntent().getExtras().getString("Account Number"));
            intent.putExtra("Password", getIntent().getExtras().getString("Password"));
            intent.putExtra("Pin", getIntent().getExtras().getString("Pin"));*/
            intent.putExtras(getIntent().getExtras());
            startActivity(intent);
        } else {
            Toast.makeText(Pin.this, "Invalid PIN", Toast.LENGTH_SHORT).show();
        }
    }

    public void withdraw() {
        if (getIntent().getExtras().getString("Pin").equals(mPin.getText().toString())) {
            Intent intent = new Intent(Pin.this, Withdraw.class);
            intent.putExtras(getIntent().getExtras());
            startActivity(intent);
        } else {
            Toast.makeText(Pin.this, "Invalid PIN", Toast.LENGTH_SHORT).show();
        }

    }
}
